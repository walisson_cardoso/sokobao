extends Node

var Padlock = preload("res://Scenes/LockLevel.tscn")

func _ready():
	$Levels.hide()
	$Options/CheckButton.pressed = OS.window_fullscreen
	$Options/HSlider.value = int(global.volume*($Options/HSlider.max_value))
	$Options/Start.grab_focus()
	global.change_track(0)
	
	for i in range(global.maxLevel+1,10+1):
		var lock = Padlock.instance()
		get_node("Levels/Level" + str(i)).add_child(lock)
		

func _on_Start_pressed():
	get_tree().change_scene("res://Scenes/Level1.tscn")
	
func _on_Levels_pressed():
	$Options.hide()
	$Levels.show()
	$Levels/Level1.grab_focus()

func _on_Quit_pressed():
	get_tree().quit()

func _on_HSlider_value_changed(value):
	global.set_volume(float(value) / $Options/HSlider.max_value)
	
func _on_CheckButton_pressed():
	OS.set_window_fullscreen(not OS.window_fullscreen)

func _on_Back_pressed():
	$Levels.hide()
	$Options.show()
	$Options/Start.grab_focus()

func _go_to_level(level):
	if level <= global.maxLevel:
		get_tree().change_scene("res://Scenes/Level" + str(level) + ".tscn")
