extends Area2D

var SPEED = 100
var player = null
var placed = false

var tile_x = 0
var tile_y = 0

func _ready():
	
	if get_parent().has_node("Player"):
		player = get_parent().get_node("Player")
		SPEED = player.SPEED
		

func placing(isOnTarget):
	if isOnTarget:
		set('modulate', Color(1,0.5,0.5,1))
	else:
		set('modulate', Color(1,1,1,1))
	placed = isOnTarget

func moveTo(next_x, next_y):
	tile_x = next_x
	tile_y = next_y
	position.x = tile_x*64+32
	position.y = tile_y*64+32