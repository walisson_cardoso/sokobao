extends CanvasLayer

func _ready():
	$Continue.grab_focus()
	$HSlider
	$CheckButton.pressed = OS.window_fullscreen
	$HSlider.value = int(global.volume*$HSlider.max_value)

func _on_Continue_pressed():
	get_tree().paused = false
	queue_free()

func _on_Reset_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()
	queue_free()

func _on_Menu_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/Menu.tscn")
	queue_free()


func _on_HSlider_value_changed(value):
	global.set_volume(float(value)/$HSlider.max_value)

func _on_CheckButton_pressed():
	OS.set_window_fullscreen(not OS.window_fullscreen)
