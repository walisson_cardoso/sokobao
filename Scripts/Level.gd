extends Node2D

var boxes = []
var nextLevel = "" # To be defined
var pauseMenu = preload("res://Scenes/PauseMenu.tscn")

func _ready():
	add_boxes()
	level_info()
	
	for box in boxes:
		box.tile_x = int(box.position.x/64)
		box.tile_y = int(box.position.y/64)

func set_music_track(trackNumber):
	global.change_track(trackNumber)

func _process(delta):
	
	if Input.is_action_just_pressed("reload"):
		get_tree().reload_current_scene()
	if Input.is_action_just_pressed("ui_accept"):
		get_parent().add_child(pauseMenu.instance())
		get_tree().paused = true
	
	if all_placed():
		get_tree().change_scene(nextLevel)

# To be overloaded
func add_boxes():
	pass

# To be overloaded
func level_info():
	pass

# To be overloaded
func all_placed():
	var are_placed = true
	for box in boxes:
		if not box.placed:
			are_placed = false
	return are_placed

func set_next_level(next):
	nextLevel = next

func cellIsFree(tile_x, tile_y, action):
	var status = $TileMap.get_cell(tile_x, tile_y)
	
	if status == -1:
		return moveBox(tile_x, tile_y, action)
	else:
		return false

func moveBox(tile_x, tile_y, action):
	var boxAt = -1
	for i in range(len(boxes)):
		if tile_x == boxes[i].tile_x and tile_y == boxes[i].tile_y:
			boxAt = i
	if boxAt == -1:
		return true
	
	var next_x = tile_x
	var next_y = tile_y
	if action == "up":
		next_y -= 1
	elif action == "down":
		next_y += 1
	elif action == "left":
		next_x -= 1
	elif action == "right":
		next_x += 1
	
	if $TileMap.get_cell(next_x, next_y) != -1:
		return false
	
	var anotherBox = -1
	for i in range(len(boxes)):
		if next_x == boxes[i].tile_x and next_y == boxes[i].tile_y:
			anotherBox = i
	if anotherBox == -1:
		boxes[boxAt].moveTo(next_x, next_y)
		return true
	else:
		return false