extends Area2D

var SPEED = 100

func _ready():
	pass

func _process(delta):
	var x = 0
	var y = 0
	var action = ""
	
	if Input.is_action_just_pressed("ui_up"):
		y -= 64
		action = "up"
	elif Input.is_action_just_pressed("ui_down"):
		y += 64
		action = "down"
	elif Input.is_action_just_pressed("ui_left"):
		x -= 64
		action = "left"
	elif Input.is_action_just_pressed("ui_right"):
		x += 64
		action = "right"
	
	if x != 0 or y != 0:
		do_movement(x,y,action)

func do_movement(x,y,action):
	var tile_x = int((position.x+x)/64)
	var tile_y = int((position.y+y)/64)
	var isFree = true
	
	if get_parent().has_method("cellIsFree"):
		isFree = get_parent().cellIsFree(tile_x,tile_y,action)
	
	if isFree:
		position += Vector2(x,y)
		if action == "up":
			$Sprite.set("global_rotation", 3*PI/2)
		if action == "down":
			$Sprite.set("global_rotation", PI/2)
		if action == "left":
			$Sprite.set("global_rotation", PI)
		if action == "right":
			$Sprite.set("global_rotation", 0)