extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func _on_Target_area_entered(area):
	if area.has_method("placing"):
		area.placing(true)


func _on_Target_area_exited(area):
	if area.has_method("placing"):
		area.placing(false)
