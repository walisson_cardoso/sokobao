extends AudioStreamPlayer

var tracks =[]
var actualTrack = 0
var volume = 1.0

var levelFile = File.new()
var save_path = "user://levelFile.save"
var maxLevel = 1

func _ready():
	tracks.append(load("res://Sound/8-Bit-Puzzler.ogg"))
	tracks.append(load("res://Sound/Puzzle-Dreams.ogg"))
	tracks.append(load("res://Sound/Puzzle-Game-5_Looping.ogg"))
	tracks.append(load("res://Sound/Mysterious-Puzzle.ogg"))
	tracks.append(load("res://Sound/Puzzle-Game-3_Looping.ogg"))
	
	set_stream(tracks[0])
	play()
	
	if not levelFile.file_exists(save_path):
		save()
	read_maxLevel()

func change_track(trackNumber):
	if trackNumber != actualTrack:
		set_stream(tracks[trackNumber])
		play()
		actualTrack = trackNumber

func set_volume(value):
	#value --> [0.0, 0.1]
	volume = value
	var reduction = (1.0-value)*20
	if reduction >= 19.9:
		reduction = 10000
	volume_db = -reduction

func set_max_level(level):
	maxLevel = level
	save()

func save():
   levelFile.open(save_path, File.WRITE)
   levelFile.store_var(maxLevel)
   levelFile.close()

func read_maxLevel():
   levelFile.open(save_path, File.READ)
   maxLevel = levelFile.get_var()
   levelFile.close()